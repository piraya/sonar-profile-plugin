package nl.piraya.sonar.plugins.profile;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import org.sonar.api.platform.Server;
import org.sonar.api.platform.ServerStartHandler;
import org.sonar.api.utils.log.Logger;
import org.sonar.api.utils.log.Loggers;

/**
 * The actual ServerStartHandler that is invoked when SonarQube is started. This plugin will read the contents of the piraya-profile.xml file and
 * post it to the web-api and then sets it to be the new default profile for the Java language.
 *
 * You can generate an up-to-date piraya-profile.xml file by simply exporting the profile from the central Sonar server using the web-api @
 * /api/qualityprofiles/backup?language=java&profileName=Piraya
 *
 * @author Onno Scheffers
 */
public class ServerStartProfileHandler implements ServerStartHandler {
   private static final Logger LOG = Loggers.get(ServerStartProfileHandler.class);

   @Override
   public void onServerStart(final Server server) {
      // We're going to communicate with the SonarQube web-api. Start a new Thread for this so the main Thread is not blocked.
      new Thread(new ProfileUploader(server.getURL())).start();
   }

   /**
    * Runnable that performs the actual upload of the Piraya Quality Profile to the SonarQube web-api and then sets the Quality Profile to be the default.
    */
   private static final class ProfileUploader implements Runnable {
      private static final String CRLF = "\r\n";
      private static final String BOUNDARY = "PIRAYABV";

      private static final String BACKUP_PARAM = "backup";
      private static final String API_RESTORE = "/api/qualityprofiles/restore";
      private static final String API_SET_DEFAULT = "/api/qualityprofiles/set_default";

      private static final String FILENAME = "piraya-profile.xml";
      private static final String PROFILE = "nl/piraya/sonar/plugins/profile/" + FILENAME;

      private final String baseUrl;

      private ProfileUploader(final String baseUrl) {
         this.baseUrl = baseUrl;
      }

      @Override
      public void run() {
         uploadProfile();
         setDefaultProfile();
      }

      /**
       * Connect tot the web-api and upload the piraya-profile.xml file.
       */
      private void uploadProfile() {
         // Setup connection to the restore-functionality of the SonarQube web-api
         String url = baseUrl + API_RESTORE;
         HttpURLConnection connection;
         try {
            connection = (HttpURLConnection) new URL(url).openConnection();
            connection.setRequestProperty ("Authorization", "Basic " + new String(Base64.getEncoder().encode("admin:admin".getBytes())));
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
         } catch (IOException e) {
            LOG.error("Unable to setup connection to " + url + ", cannot import Piraya quality profile", e);
            return;
         }

         // Post the binary to the web-api
         LOG.info("Uploading Piraya Quality Profile to web-api at " + url + "...");
         try (InputStream profileStream = ServerStartProfileHandler.class.getClassLoader().getResourceAsStream(PROFILE);
              OutputStream output = connection.getOutputStream();
              PrintWriter writer = new PrintWriter(new OutputStreamWriter(output, StandardCharsets.UTF_8.name()), true)) {

            writer.append("--" + BOUNDARY).append(CRLF);
            writer.append("Content-Disposition: form-data; name=\"").append(BACKUP_PARAM).append("\"; ");
            writer.append("filename=\"").append(FILENAME).append("\"").append(CRLF);
            writer.append("Content-Type: ").append(URLConnection.guessContentTypeFromName(FILENAME)).append(CRLF);
            writer.append("Content-Transfer-Encoding: binary").append(CRLF);
            writer.append(CRLF).flush();

            // Send profile file.
            int length;
            byte[] buffer = new byte[1024];
            while ((length = profileStream.read(buffer)) != -1) {
               output.write(buffer, 0, length);
            }
            output.flush();
            writer.append(CRLF).flush();

            // End of multipart/form-data.
            writer.append("--" + BOUNDARY + "--").append(CRLF).flush();

            // Check result
            int code = connection.getResponseCode();
            if(code == 200) {
               LOG.info("Successfully imported Piraya Quality Profile");
            } else {
               LOG.error("Web-api did not accept quality profile, HTTP response code: " + code);
            }
         } catch (IOException e) {
            LOG.error("Unable to import Piraya Quality Profile due to exception", e);
         }
      }

      /**
       * Connect tot the web-api and set the Piraya profile to be the new default for the Java language.
       */
      private void setDefaultProfile() {
         // Setup connection to the set-default-functionality of the SonarQube web-api
         String url = baseUrl + API_SET_DEFAULT;
         HttpURLConnection connection;
         try {
            connection = (HttpURLConnection) new URL(url).openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty ("Authorization", "Basic " + new String(Base64.getEncoder().encode("admin:admin".getBytes())));
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            byte[] out = "language=java&profileName=Piraya".getBytes(StandardCharsets.UTF_8);
            connection.setFixedLengthStreamingMode(out.length);

            LOG.info("Setting Piraya Quality Profile as default using " + url + "...");
            connection.connect();
            try(OutputStream output = connection.getOutputStream()) {
               output.write(out);
               output.flush();
            }

            // Check result
            int code = connection.getResponseCode();
            if(code == 204) {
               LOG.info("Successfully set Piraya Quality Profile as default");
            } else {
               LOG.error("Web-api did not accept quality profile as default, HTTP response code: " + code);
            }
         } catch (IOException e) {
            LOG.error("Unable to set Piraya Quality Profile as default due to exception", e);
         }
      }
   }
}
