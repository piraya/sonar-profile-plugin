package nl.piraya.sonar.plugins.profile;

import org.sonar.api.Plugin;

/**
 * SonarQube plugin that simply registers a new ServerStartHandler extension point.
 *
 * @author Onno Scheffers
 */
public class ProfilePlugin implements Plugin {
   @Override
   public void define(final Context context) {
      // Register our custom ServerStartHandler extension point
      context.addExtension(new ServerStartProfileHandler());
   }
}
