## SonarQube Piraya Profile Plugin ##

### Imports Piraya Quality Profile on SonarQube Startup ###
 
If you just want to do a quick Sonar validation of your project locally and you're not
interested in maintaining a history of the projects' code quality over time, you can
easily create a Dockerfile using the default
[SonarQube Docker Image](https://hub.docker.com/_/sonarqube/), add the plugins you
need and run a Sonar validation against your project.... as long as your are happy
with the default rules that is.

Problem is that the default SonarQube Docker Image uses an embedded H2 database
which typically does not survive a container restart. So if you import your own
quality profile or customize the rules, your changes are lost as soon as you
restart the container.

The default H2 database is not recommended for production use, but it makes it very
easy to quickly have a SonarQube server up-and-running without any further
configuration.

This plugin was created to automatically import
the [Piraya B.V.](https://www.piraya.nl) Quality Profile when the SonarQube server
starts up and then sets it to be the default Java profile so it is immediately
ready for use. This allows you to validate your code locally before committing your
changes or to quickly generate a report using the same setting and plugins your
central SonarQube server uses.

This plugin was created explicitly for use in the
[Piraya SonarQube Docker image](https://hub.docker.com/r/pirayabv/sonar/). But
we decided to share the code so others could use this project as a basis for their
own auto-import quality profile plugin.
